

package app.com.weatherdata_kotlin_mvvm.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import app.com.weatherdata_kotlin_mvvm.data.persistence.local.WeatherDB
import app.com.weatherdata_kotlin_mvvm.data.repositoryImpl.WeatherRepoImpl
import app.com.weatherdata_kotlin_mvvm.data.utils.ConnectionUtils
import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository
import app.com.weatherdata_kotlin_mvvm.domain.usecase.GetForecastUseCase
import app.com.weatherdata_kotlin_mvvm.presentation.viewmodel.WeatherViewModel
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.internal.util.NotificationLite.getValue
import io.reactivex.observers.TestObserver
import junit.framework.Assert
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class WeatherViewModelTest {

    private lateinit var appDatabase: WeatherDB
    private lateinit var viewModel: WeatherViewModel
    private lateinit var utils:ConnectionUtils
    lateinit var observableResult: Observable<GetForecastUseCase.Result>


    @Mock
    private lateinit var result: GetForecastUseCase.Result

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private fun Result(): GetForecastUseCase.Result {
        return result
    }


    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, WeatherDB::class.java).build()

        utils = ConnectionUtils(context)

        val weatherRepo = WeatherRepoImpl.getInstance(appDatabase.weatherDao(),utils)

        viewModel = WeatherViewModel(weatherRepo,context)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDefaultValues() {
        assertFalse(getValue(viewModel.getWeather()==null))
    }

}