package app.com.weatherdata_kotlin_mvvm.data

import android.app.Activity
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import java.util.*

/**
 * [Plant] objects used for tests.
 */
val testData = arrayListOf(
    DarkskyModel.Data(100993383L, "Clear Sunny day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
          100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Partly Sunny day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Partly Cludy day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Rain showers", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Clear Sunny day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Clear Sunny day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L),
    DarkskyModel.Data(100993383L, "Clear Sunny day", "clear-day", 22,12,
        23.00, 86.00,87.00,90.00,89.00,90.00,
        100993383L,100993383L,12.00,45.00,103.00,34.00,
        45.00,100993383L,12,34.00,25,100993383L,27.00,12.00,100993383L,
        100993383L)
)
