package app.com.weatherdata_kotlin_mvvm.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.platform.app.InstrumentationRegistry
import app.com.weatherdata_kotlin_mvvm.data.persistence.dao.WeatherDao
import app.com.weatherdata_kotlin_mvvm.data.persistence.local.WeatherDB
import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.util.NotificationLite.getValue
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers
import org.junit.*

class WeatherDaoTest {
    private lateinit var database: WeatherDB
    private lateinit var weatherDao: WeatherDao
    private lateinit var dataFromDb:List<DarkskyModel.Data>


    private val day1 = DarkskyModel.Data(
        100993383L, "Clear Sunny day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day2 = DarkskyModel.Data(
        100993383L, "Partly Sunny day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day3 = DarkskyModel.Data(
        100993383L, "Partly Cludy day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day4 = DarkskyModel.Data(
        100993383L, "Rain showers", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day5 = DarkskyModel.Data(
        100993383L, "Clear Sunny day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day6 = DarkskyModel.Data(
        100993383L, "Clear Sunny day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )
    private val day7 = DarkskyModel.Data(
        100993383L, "Clear Sunny day", "clear-day", 22, 12,
        23.00, 86.00, 87.00, 90.00, 89.00, 90.00,
        100993383L, 100993383L, 12.00, 45.00, 103.00, 34.00,
        45.00, 100993383L, 12, 34.00, 25, 100993383L, 27.00, 12.00, 100993383L,
        100993383L
    )


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, WeatherDB::class.java).build()
        weatherDao = database.weatherDao()
        //insert dummy data
        database.weatherDao().insertAll(listOf(day1, day2, day3, day4, day5, day6, day7))
    }

    @After
    fun closeDb() {
        database.close()
    }

    //Query database and check if we get weatherList count :>7
    @Test
    fun testWeatherCount() {
        val weatherList = weatherDao.queryWeatherData(7, 0).toObservable()
        weatherList .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                getValue(it)
            }, onError = {
                    e -> handleError(e) }
            )
    }

    fun getValue(list: List<DarkskyModel.Data>){
        dataFromDb = list
        Assert.assertEquals(dataFromDb.size, Matchers.equalTo(7))
    }

    @SuppressLint("StringFormatInvalid")
    fun handleError(e:Throwable){
        Log.e("onError", e.printStackTrace().toString())
    }
}