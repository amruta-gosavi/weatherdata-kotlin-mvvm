package app.com.weatherdata_kotlin_mvvm


import android.app.Application
import androidx.room.Database
import androidx.room.Room
import app.com.weatherdata_kotlin_mvvm.data.persistence.local.WeatherDB

class WeatherApp : Application() {

    companion object {
        var weatherDB: WeatherDB? = null
    }

    override fun onCreate() {
        super.onCreate()
    }
}
