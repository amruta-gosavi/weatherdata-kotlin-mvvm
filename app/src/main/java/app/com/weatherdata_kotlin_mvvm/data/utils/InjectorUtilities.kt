package app.com.weatherdata_kotlin_mvvm.data.utils

import android.content.Context
import app.com.weatherdata_kotlin_mvvm.data.persistence.local.WeatherDB
import app.com.weatherdata_kotlin_mvvm.data.repositoryImpl.WeatherRepoImpl
import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository
import app.com.weatherdata_kotlin_mvvm.presentation.viewmodel.WeatherViewModelFactory

/**
 * Static methods used to inject classes needed for various Activities and Fragments.
 */
object InjectorUtils {

    private fun getConnectionUtils(context: Context): ConnectionUtils {

        return ConnectionUtils(context)
    }

    private fun getWeatherRepository(context: Context): WeatherRepository {
        return WeatherRepoImpl.getInstance(
            WeatherDB.getInstance(context.applicationContext).weatherDao(),
            getConnectionUtils(context)
        )
    }

    fun provideWeatherViewModelFactory(
        context: Context
    ): WeatherViewModelFactory {
        val repository = getWeatherRepository(context)
        return WeatherViewModelFactory(repository, context)
    }
}
