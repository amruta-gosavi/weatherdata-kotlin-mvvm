/**
 * Automatically generated file. DO NOT MODIFY
 */
package app.com.weatherdata_kotlin_mvvm.data;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String VERSION_NAME = "1.00";
  // Fields from build type: debug
  public static final String Endpoint = "https://api.darksky.net";
}
