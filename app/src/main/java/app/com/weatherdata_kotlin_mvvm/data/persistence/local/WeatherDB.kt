package app.com.weatherdata_kotlin_mvvm.data.persistence.local



import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import app.com.weatherdata_kotlin_mvvm.data.persistence.dao.WeatherDao
import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import app.com.weatherdata_kotlin_mvvm.data.utils.Constants

@Database(entities = [DarkskyModel.Data::class], version = 1, exportSchema = false)
abstract class WeatherDB : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: WeatherDB? = null

        fun getInstance(context: Context): WeatherDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): WeatherDB {
            return Room.databaseBuilder(context, WeatherDB::class.java, Constants.DATABASE_NAME)
                .build()
        }
    }

}