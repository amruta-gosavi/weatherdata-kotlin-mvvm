package app.com.weatherdata_kotlin_mvvm.data.datamapper
import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import app.com.weatherdata_kotlin_mvvm.data.utils.DateTimeUtils
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.WeatherUnits
import java.text.SimpleDateFormat

class WeatherDataMapper {

    /**
     * Apply business rules and transform model into displayable viewmodel
     * Process weather data and apply units/rules as needed
     *
     * @param EntityCollection Object Collection to be transformed.
     * @return
     */

    fun transform(item: DarkskyModel.Darksky): DisplayableWeatherData.DisplayableDarkSky {
        var currently = transformDisplayableData(item.currently)

        var displayableData: MutableList<DisplayableWeatherData.DisplayableData> =
            ArrayList<DisplayableWeatherData.DisplayableData>()

        if (!item.daily.data.isNullOrEmpty()) {
            for (dataItem in item.daily.data) {
                var data = transformDisplayableData(dataItem)
                displayableData.add(data)
            }

        }
        var daily = DisplayableWeatherData.DisplayableDaily(
            item.daily.summary,
            item.daily.icon,
            displayableData
        )

        val displayableWeather = DisplayableWeatherData.DisplayableDarkSky(
            currently, daily
        )
        return displayableWeather
    }

    fun transformDisplayableData(item: DarkskyModel.Data): DisplayableWeatherData.DisplayableData {
        val displayableData = DisplayableWeatherData.DisplayableData(
            getTimeToDateString(item.time),
            item.summary,
            item.icon,
            getTemprature(item.temperature, item.apparentTemperatureHigh),
            item.dewPoint?.toString(),
            getHumidity(item.humidity),
            getWindSpeed(item.windSpeed),
            getCloudCover(item.cloudCover),
            getUVIndex(item.uvIndex),
            "" + item.visibility,
            "" + item.ozone,
            getPrecipitationProbability(item.precipProbability),
            getDay(item.time),
            getGusts(item.windGust),
            getSunrise(item.sunriseTime),
            getSunset(item.sunsetTime),
            getPrecipIntensity(item.precipIntensity),
            getPressure(item.pressure),
            getMonth(item.time)
        )
        return displayableData
    }

    fun transformFromDBData(dbData: List<DarkskyModel.Data>):DisplayableWeatherData.DisplayableDarkSky{
        var displayableData: MutableList<DisplayableWeatherData.DisplayableData> =
            ArrayList<DisplayableWeatherData.DisplayableData>()

        if (!dbData.isNullOrEmpty()) {
            for (dataItem in dbData) {
                var data = transformDisplayableData(dataItem)
                displayableData.add(data)
            }
        }

        var daily = DisplayableWeatherData.DisplayableDaily(displayableData.get(0).summary,displayableData.get(0).icon,displayableData as MutableList<DisplayableWeatherData.DisplayableData>)

        var currently = displayableData.get(0)
        var displayableModel = DisplayableWeatherData.DisplayableDarkSky(currently,daily)

        return displayableModel
    }


    fun getTimeToDateString(time: Long): String? {
        var timeString: String? = null
        timeString = DateTimeUtils.convertLongToDateString(time,DateTimeUtils.FORMAT_CURRENT)
        return timeString
    }

    fun getTemprature(temp: Double?, apparentHigh: Double?): String? {
        var temperature = ""
        if (temp == null) {
            temperature = "" + apparentHigh + " \u2109"
        } else
            temperature = "" + temp + " \u2109"

        return temperature
    }

    fun getHumidity(humidity: Double?): String? {
        var humidityPercent = humidity?.times(100)
        var humidityString = convertDoubleTwoDecimalPoints("" + humidityPercent) + WeatherUnits.PERCENT.value
        return humidityString
    }

    fun getWindSpeed(wind: Double?): String? {
        var windSpeed = convertDoubleTwoDecimalPoints("" + wind)+" " + WeatherUnits.MPH.value
        return windSpeed
    }

    fun getCloudCover(cloudCover: Double?): String? {
        var cloud = ""
        if (cloudCover != null) {
            var cloudPercent = cloudCover.times(100)
            cloud = convertDoubleTwoDecimalPoints("" + cloudPercent) + WeatherUnits.PERCENT.value
        }
        return cloud
    }

    fun getPrecipitationProbability(precip: Double?): String? {
        var precipitation = ""
        if (precip != null) {
            var cloudPercent = precip.times(100)
            precipitation = "" + convertDoubleTwoDecimalPoints("" + cloudPercent)+ WeatherUnits.PERCENT.value
        }

        return precipitation
    }

    fun convertDoubleTwoDecimalPoints(number: String?): String {
        val d = number?.toDouble()
        return "%.2f".format(d)
    }

    fun getUVIndex(uvIndex: Int?): String? {
        var uvIndexDesc =WeatherUnits.NO_UNIT
        if (uvIndex != null) {
            if (uvIndex > 6) {
                uvIndexDesc = WeatherUnits.HIGH
            }
        }
        if (uvIndex == 5) {
            uvIndexDesc = WeatherUnits.MODERATE
        }
        if (uvIndex != null) {
            if (uvIndex < 5) {
                uvIndexDesc = WeatherUnits.LOW
            }
        }

    return "UV Index:  " + uvIndexDesc
}

    fun getDay(time: Long):String?{
        var timeString: String? = null
        timeString = DateTimeUtils.convertLongToDateString(time,DateTimeUtils.FORMAT_DAY)
        return timeString
    }

    fun getMonth(time:Long):String?{
        var timeString: String? = null
        timeString = DateTimeUtils.convertLongToDateString(time,DateTimeUtils.Format_Month)
        return timeString
    }

    fun getGusts(gustVal: Double?):String?{
        var gusts:String = ""
        if (gustVal != null) {
            var cloudPercent = gustVal.times(100)
            gusts =
                "" + convertDoubleTwoDecimalPoints("" + cloudPercent)
        }

        return gusts
    }

    fun getSunrise(time: Long?):String?{
        var timeString: String? = ""
        if(time!=null){
            timeString = DateTimeUtils.convertLongToDateString(time,DateTimeUtils.FORMAT_CURRENT)
        }
        return timeString
    }


    fun getSunset(time: Long?):String?{
        var timeString: String? = ""
        if(time!=null){
            timeString = DateTimeUtils.convertLongToDateString(time,DateTimeUtils.FORMAT_CURRENT)
        }
        return timeString
    }


    fun getPrecipIntensity(precip: Double?):String?{
        var precipitation = ""
        if (precip != null) {
            var cloudPercent = precip.times(100)
            precipitation = "" + convertDoubleTwoDecimalPoints("" + cloudPercent)
        }

        return precipitation
    }

    fun getPressure(pressure: Double?):String?{
        var precipitation = ""
        if (pressure != null) {
            var cloudPercent = pressure.times(100)
            precipitation = "" + convertDoubleTwoDecimalPoints("" + cloudPercent)
        }

        return precipitation
    }

}