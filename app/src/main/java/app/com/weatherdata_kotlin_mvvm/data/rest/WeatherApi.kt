package app.com.weatherdata_kotlin_mvvm.data.rest


import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface WeatherApi {

    @GET("forecast/{key}/{latitude},{longitude}")
    fun getForcast(@HeaderMap headers: Map<String, String>, @Path(value = "key") key: String,@Path(value = "latitude") latitude: String,
                   @Path(value = "longitude") longitude: String): Observable<DarkskyModel.Darksky>

}