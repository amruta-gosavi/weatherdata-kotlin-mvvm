package app.com.weatherdata_kotlin_mvvm.data.repositoryImpl

import app.com.weatherdata_kotlin_mvvm.data.RestApi
import app.com.weatherdata_kotlin_mvvm.data.datamapper.WeatherDataMapper
import app.com.weatherdata_kotlin_mvvm.data.persistence.dao.WeatherDao
import app.com.weatherdata_kotlin_mvvm.data.utils.ConnectionUtils
import app.com.weatherdata_kotlin_mvvm.data.utils.Constants
import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import io.reactivex.Observable

/**
 * Implementation of WeatherRepository: The data is emitted either from api or from db(sqllite)
 */
class WeatherRepoImpl(val weatherDao: WeatherDao, val utils: ConnectionUtils) : WeatherRepository {
    private val weatherDataMapper = WeatherDataMapper()


    override fun getWeatherForecast(
        lat: Double,
        lon: Double
    ): Observable<DisplayableWeatherData.DisplayableDarkSky> {
        val hasConnection = utils.isConnectedToInternet()
        if (hasConnection) {
            return getWeatherForecastFromAPI(lat, lon)
        } else {
            return getWeatherForecastFromDB(7, 0)
        }
    }

    companion object {

        // For Singleton instantiation
        @Volatile
        private var instance: WeatherRepository? = null

        fun getInstance(weatherDao: WeatherDao, utils: ConnectionUtils) =
            instance ?: synchronized(this) {
                instance ?: WeatherRepoImpl(weatherDao, utils).also { instance = it }
            }
    }


    private fun getWeatherForecastFromAPI(
        lat: Double,
        lon: Double
    ): Observable<DisplayableWeatherData.DisplayableDarkSky> {
        return RestApi.getInstance().weatherApi.getForcast(
            RestApi.getInstance().headers, Constants.API_KEY, lat.toString(), lon.toString()
        )
            .doOnNext {
                weatherDao.insertAll(it.daily.data)
            }
            .map { weatherDataMapper.transform(it) }

    }

    private fun getWeatherForecastFromDB(
        limit: Int,
        offset: Int
    ): Observable<DisplayableWeatherData.DisplayableDarkSky> {
        return weatherDao.queryWeatherData(limit, offset).toObservable()
            .map { weatherDataMapper.transformFromDBData(it) }
    }
}
