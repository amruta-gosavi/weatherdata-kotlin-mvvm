package app.com.weatherdata_kotlin_mvvm.data;

import android.os.Build;
import android.util.Log;

import com.moczul.ok2curl.CurlInterceptor;
import com.moczul.ok2curl.logger.Loggable;

import java.util.LinkedHashMap;
import java.util.Map;

import app.com.weatherdata_kotlin_mvvm.data.rest.WeatherApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestApi {

    private String REST_ENDPOINT = BuildConfig.Endpoint;
    private WeatherApi weatherApi;
    private static volatile RestApi instance;
    private Retrofit retrofit;
    private final String OS_VERSION = getAndroidVersion();
    private final String VERSION_NUMBER = "5";
    private final String VERSION_NAME =BuildConfig.VERSION_NAME;
    private final String DEVICE_NAME = "";

    private final String USER_AGENT = "Android;"+VERSION_NUMBER+";"+OS_VERSION+";"+VERSION_NAME+";"+DEVICE_NAME;

    private void initRetrofit() {
        if (retrofit == null) {
           // initUserAgent();
            if(BuildConfig.DEBUG){
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                OkHttpClient client = new OkHttpClient.Builder()
                        .addInterceptor(new UserAgentInterceptor(USER_AGENT)).addInterceptor(interceptor)
                        .addInterceptor(new CurlInterceptor(new Loggable() {
                            @Override
                            public void log(String message) {
                                Log.v("Ok2Curl", message);
                            }
                        }))
                        .build();
                retrofit = new Retrofit.Builder()
                        .baseUrl(REST_ENDPOINT)
                        .client(client)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }else {
                retrofit = new Retrofit.Builder()
                        .baseUrl(REST_ENDPOINT)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }
    }

    public WeatherApi getWeatherApi() {
        initRetrofit();
        if (weatherApi == null) {
            weatherApi = retrofit.create(WeatherApi.class);
        }
        return weatherApi;
    }

    public static RestApi getInstance() {
        if (instance == null) {
            synchronized (RestApi.class) {
                if (instance == null) {
                    instance = new RestApi();
                }
            }
        }
        return instance;
    }

    private RestApi() {
        initRetrofit();
        createHeaders();
    }

    private Map<String, String> headers = new LinkedHashMap<>();
    private Map<String, String> sessionHeaders = new LinkedHashMap<>();

    public Map<String, String> getHeaders() {
        return headers;
    }

    private void createHeaders() {
        headers.put("Accept", "*/*");
    }

    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        return release;
    }
}
