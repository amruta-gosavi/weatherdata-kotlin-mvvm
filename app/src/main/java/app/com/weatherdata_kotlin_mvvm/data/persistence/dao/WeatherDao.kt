package app.com.weatherdata_kotlin_mvvm.data.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import app.com.weatherdata_kotlin_mvvm.data.rest.model.DarkskyModel
import io.reactivex.Single

@Dao
interface WeatherDao {

    @Query("SELECT * FROM weatherdata ORDER BY time limit :limit offset :offset")
    fun queryWeatherData(limit: Int, offset: Int): Single<List<DarkskyModel.Data>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(data: List<DarkskyModel.Data>?)
}