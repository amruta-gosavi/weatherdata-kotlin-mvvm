package app.com.weatherdata_kotlin_mvvm.presentation.viewmodel

import android.content.Context
import android.location.Location
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.data.utils.Constants
import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository
import app.com.weatherdata_kotlin_mvvm.domain.usecase.GetForecastUseCase
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.PermissionUtils
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import io.reactivex.Observable

/**
 * ViewModel triggers usecases and has Observable<Result> which observes data & updates views
 * The ViewModel used in [WeatherNowFragment and WeatherDailyFragment]
 */
class WeatherViewModel(private val repository: WeatherRepository, private val context: Context) :
    ViewModel() {

    private val getForecastUseCase: GetForecastUseCase = GetForecastUseCase(repository)
    lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var observableResult:Observable<GetForecastUseCase.Result>
    private var userLocation: Location = Constants.NEW_YORK_LOCATION

    init {
        getUserLocation()
    }

    fun getWeather(): Observable<GetForecastUseCase.Result> {
        observableResult =  getForecastUseCase.execute(userLocation.latitude, userLocation.longitude)
        return observableResult
    }

    fun getUserLocation() {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        if (PermissionUtils.checkUserLocationPermission(context)) {
            //permission granted
            listenLocation()

        } else {
            //Permission denied!! Displaying default location data.
            getWeather()
            Toast.makeText(
                context,
                context.resources.getString(R.string.alert_permission_denied),
                Toast.LENGTH_SHORT
            ).show();
        }
    }

    fun listenLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    userLocation = location
                    getWeather()
                }
            }
    }


}