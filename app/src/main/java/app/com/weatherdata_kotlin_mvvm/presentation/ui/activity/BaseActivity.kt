package app.com.weatherdata_kotlin_mvvm.presentation.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import app.com.weatherdata_kotlin_mvvm.R
import butterknife.BindView
import butterknife.ButterKnife
import org.jetbrains.annotations.Nullable

/**
 * BaseActivity: handles default functionality like update title,toolbar
 */
abstract class BaseActivity : AppCompatActivity() {

    @Nullable
    @BindView(R.id.tv_error)
    lateinit var tvError: TextView

    @Nullable
    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @Nullable
    @BindView(R.id.toolbar_title)
    lateinit var tbtitle: TextView

    @Nullable
    @BindView(R.id.progressBar)
    lateinit var progressBar: View

    fun updateTitle(title: String) {
        tbtitle.text = title
        tbtitle.visibility = View.VISIBLE
    }

    fun updateTitle(title: Int) {
        tbtitle.setText(title)
        tbtitle.visibility = View.VISIBLE
    }

    abstract fun popMe()

    fun showProgress(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this@BaseActivity)
    }
}
