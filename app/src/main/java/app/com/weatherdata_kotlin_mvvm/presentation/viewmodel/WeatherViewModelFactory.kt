package app.com.weatherdata_kotlin_mvvm.presentation.viewmodel


import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository

/**
 * This class generates viewmodel classes
 *
 */
class WeatherViewModelFactory(
    private val repository: WeatherRepository, private val context: Context
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return WeatherViewModel(repository, context) as T
    }
}