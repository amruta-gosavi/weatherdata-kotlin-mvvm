package app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.data.utils.InjectorUtils
import app.com.weatherdata_kotlin_mvvm.domain.usecase.GetForecastUseCase
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.adapters.DailyForecastAdapter
import app.com.weatherdata_kotlin_mvvm.presentation.ui.custom.RecycleLayoutManager
import app.com.weatherdata_kotlin_mvvm.presentation.ui.custom.ScrollInterface
import app.com.weatherdata_kotlin_mvvm.presentation.ui.listeners.ActionListener
import app.com.weatherdata_kotlin_mvvm.presentation.viewmodel.WeatherViewModel
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * This fragments observes daily forecast and renders UI for showing weather daily data.
 */
class WeatherDailyFragment : BaseFragment(), ScrollInterface, SwipeRefreshLayout.OnRefreshListener,
    ActionListener {

    lateinit var unbinder: Unbinder
    lateinit var mViewModel: WeatherViewModel
    val observerList = ArrayList<DisplayableWeatherData.DisplayableDarkSky>()
    val disposables = CompositeDisposable()
    var mDailyForecastAdapter: DailyForecastAdapter? = null

    @BindView(R.id.tv_title) lateinit var tv_title: TextView
    @BindView(R.id.refreshLayout) lateinit var refreshLayout: SwipeRefreshLayout
    @BindView(R.id.recyclerView) lateinit var recyclerView: RecyclerView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_daily_fragment
    }

    override fun initializeLayout() {
        showToolbar(true)
        setStatusBarColor(R.color.colorPrimaryDark)
        interactionListener?.updateTitle(R.string.app_name)
        refreshLayout.setOnRefreshListener(this@WeatherDailyFragment)
        refreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimary,
            R.color.colorPrimary,
            R.color.colorPrimary
        )
        initializeAdapter()
        subscribeUi()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var factory = InjectorUtils.provideWeatherViewModelFactory(requireContext())
        mViewModel = ViewModelProviders.of(this, factory)
            .get(WeatherViewModel::class.java)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun subscribeUi() {
        mViewModel.getWeather()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleResult(it) }
            .addTo(disposables)
    }

    fun setData(data: ArrayList<DisplayableWeatherData.DisplayableDarkSky>) {
        if (data.size != 0) {
            var dailyData = data?.get(0).daily.data
            tv_title.setText(dailyData?.get(0)?.month)
            refreshLayout.visibility = View.VISIBLE
            val adapter = mDailyForecastAdapter
            recyclerView.adapter = adapter
            adapter?.clearAll()
            adapter?.addItems(dailyData as List<DisplayableWeatherData.DisplayableData>)
            adapter?.notifyDataSetChanged()
        }
    }

    // Handles Result
    private fun handleResult(result: Any) {
        stopLoadingAnimation()
        when (result) {
            is GetForecastUseCase.Result.Success -> {
                observerList.clear()
                observerList.addAll(listOf(result.weather))
                setData(observerList)
            }
            is GetForecastUseCase.Result.Failure -> {
                showError("Something went wrong! Please try again  later")
            }
        }
    }

    fun initializeAdapter() {
        mDailyForecastAdapter = DailyForecastAdapter(context!!, this)
        recyclerView.setHasFixedSize(true)
        val layoutManager = RecycleLayoutManager(this.activity!!)
        layoutManager.setScrollInterface(this)
        recyclerView.layoutManager = layoutManager
    }

    override fun onViewClicked(parentId: String) {

        if (observerList.get(0).daily != null && !observerList.get(0).daily.data.isNullOrEmpty())
            fragmentNavigation?.pushFragment(WeatherViewPager(observerList.get(0).daily?.data!!))
    }

    fun stopLoadingAnimation() {
        refreshLayout.isRefreshing = false
    }

    fun showError(error: String) {
        showToast(error)
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onRefresh() {
        subscribeUi()
    }

    override fun overscrolled() {
        // Note: In future load data for next 7 days (Currently, API is not supported for more than 7 days)
    }


}



