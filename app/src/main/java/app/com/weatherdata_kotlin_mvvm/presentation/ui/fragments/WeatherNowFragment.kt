package app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.data.utils.ConnectionUtils
import app.com.weatherdata_kotlin_mvvm.data.utils.Constants
import app.com.weatherdata_kotlin_mvvm.data.utils.InjectorUtils
import app.com.weatherdata_kotlin_mvvm.domain.usecase.GetForecastUseCase
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.WeatherType
import app.com.weatherdata_kotlin_mvvm.presentation.viewmodel.WeatherViewModel
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.util.*


/**
 * This is the main fragment which  renders/observes current weather values using ViewModel
 */
class WeatherNowFragment : BaseFragment(), SwipeRefreshLayout.OnRefreshListener{

    lateinit var unbinder: Unbinder
    lateinit var mViewModel: WeatherViewModel
    val observerList = ArrayList<DisplayableWeatherData.DisplayableDarkSky>()
    val disposables = CompositeDisposable()
    private var userLocation: Location = Constants.NEW_YORK_LOCATION

    @BindView(R.id.tv_time) lateinit var tv_time: TextView
    @BindView(R.id.tv_temp) lateinit var tv_temp: TextView
    @BindView(R.id.tv_weather) lateinit var tv_weather: TextView
    @BindView(R.id.tv_humidity) lateinit var tv_humidity: TextView
    @BindView(R.id.tv_uv_index) lateinit var tv_uv_index: TextView
    @BindView(R.id.tv_wind) lateinit var tv_wind: TextView
    @BindView(R.id.tv_warning) lateinit var tv_warning: TextView
    @BindView(R.id.iv_weather) lateinit var iv_weather: ImageView
    @BindView(R.id.tv_summary) lateinit var tv_summary: TextView
    @BindView(R.id.tv_cloud_cover) lateinit var tv_cloud_cover: TextView
    @BindView(R.id.tv_precipitation) lateinit var tv_precipitation: TextView
    @BindView(R.id.refreshLayout) lateinit var refreshLayout: SwipeRefreshLayout


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_weather_now
    }

    override fun initializeLayout() {
        showToolbar(true)
        setStatusBarColor(R.color.colorPrimaryDark)
        interactionListener?.updateTitle(R.string.app_name)
        refreshLayout.setOnRefreshListener(this@WeatherNowFragment)
        refreshLayout.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimary,
            R.color.colorPrimary,
            R.color.colorPrimary
        )

        subscribeUi()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var factory = InjectorUtils.provideWeatherViewModelFactory(requireContext())
        mViewModel = ViewModelProviders.of(this, factory).get(WeatherViewModel::class.java)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun subscribeUi() {
        mViewModel.getWeather()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleResult(it) }
            .addTo(disposables)
    }

    // Handles Result
    fun handleResult(result: Any) {
        stopLoadingAnimation()
        when (result) {
            is GetForecastUseCase.Result.Success -> {
                observerList.clear()
                observerList.addAll(listOf(result.weather))
                setData(observerList)
            }
            is GetForecastUseCase.Result.Failure -> {
                if (!ConnectionUtils(context!!).isConnectedToInternet()) {
                    showError(context!!.resources.getString(R.string.alert_no_internet))
                } else
                    showError(context!!.resources.getString(R.string.alert_error))
            }
            is GetForecastUseCase.Result.Loading -> {
            }
        }
    }

    fun setData(data: ArrayList<DisplayableWeatherData.DisplayableDarkSky>) {
        if (data.size != 0) {
            tv_temp.text = data.get(0).currently.temperature
            tv_humidity.text = context?.resources?.getString(R.string.title_humidity)+"  "  + data.get(0).currently.humidity
            tv_uv_index.text = data.get(0).currently.uvIndexTime
            tv_time.text = data.get(0).currently.time
            tv_wind.text = context?.resources?.getString(R.string.title_wind)+"  " + data.get(0).currently.windSpeed
            tv_weather.text = data.get(0).currently?.summary
            tv_warning.text = data.get(0).daily.data?.get(0)?.summary
            tv_summary.text = data.get(0).daily.summary
            tv_cloud_cover.text =
                context?.resources?.getString(R.string.title_cloud_cover) + " " + data.get(0).currently.cloudCover
            tv_precipitation.text =
                context?.resources?.getString(R.string.chance_of_precipitation) + data.get(0).currently.precipProbability

            when (data.get(0).currently.icon) {
                WeatherType.Clear_day.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_clear_day)
                }
                WeatherType.Partly_cloudy_day.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_partly_cloudy_day)
                }
                WeatherType.Partly_cloudy_night.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_party_cloudy_night)
                }
                WeatherType.Rain.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_rain)
                }
                WeatherType.Partly_cloudy_night.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_party_cloudy_night)
                }
                WeatherType.Clear_night.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_clear_night)
                }
            }
        }
    }


    fun setImageDrawables(drawable: Int) {
        iv_weather.setImageResource(drawable)
        var img = ContextCompat.getDrawable(context!!, drawable)
        img?.setBounds(0, 0, 60, 60)
        tv_warning.setCompoundDrawables(img, null, null, null)

    }

    override fun onDestroy() {
        //cleanup
        disposables.clear()
        super.onDestroy()
    }

    fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun onRefresh() {
        subscribeUi()
    }

    fun stopLoadingAnimation() {
        refreshLayout.isRefreshing = false
    }
}



