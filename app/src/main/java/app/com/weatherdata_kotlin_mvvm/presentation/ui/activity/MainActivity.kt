package app.com.weatherdata_kotlin_mvvm.presentation.ui.activity

import android.os.Bundle
import android.os.Handler
import android.util.Pair
import android.view.MenuItem
import androidx.fragment.app.Fragment
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.presentation.support.Builder
import app.com.weatherdata_kotlin_mvvm.presentation.support.FragmentController
import app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments.BaseFragment
import app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments.WeatherDailyFragment
import app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments.WeatherNowFragment
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : BaseActivity(), BaseFragment.FragmentInteractionListener,
    FragmentController.TransactionListener,
    FragmentController.RootFragmentListener, BaseFragment.FragmentNavigation {

    private var fragmentController: FragmentController? = null
    private var lastReselection: Pair<Long, Int>? = null

    @BindView(R.id.navigation)
    lateinit var navigation: BottomNavigationView


    override fun popMe() {
        showProgress(false)
        if (!fragmentController!!.isRootFragment) {
            fragmentController?.popFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this@MainActivity)

        navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        navigation.setOnNavigationItemReselectedListener(onNavigationItemReselectedListener)
        fragmentController = Builder.newBuilder(
            savedInstanceState,
            supportFragmentManager, R.id.content
        )
            .transactionListener(this@MainActivity)
            .rootFragmentListener(this@MainActivity, 3)
            .build()

        fragmentController =
            Builder.newBuilder(
                savedInstanceState,
                supportFragmentManager,
                R.id.content
            )
                .transactionListener(this@MainActivity)
                .rootFragmentListener(this@MainActivity, 3)
                .build()
        getRootFragment(FragmentController.TAB1)
    }

    fun openFragment() {
        supportFragmentManager.beginTransaction().add(
            R.id.fragment_container,
            WeatherNowFragment()
        ).commit()
    }

    private val onNavigationItemReselectedListener =
        object : BottomNavigationView.OnNavigationItemReselectedListener {

            override fun onNavigationItemReselected(p0: MenuItem) {
                if (lastReselection == null) {
                    lastReselection = Pair(System.currentTimeMillis(), p0.getItemId())
                    clearLastReselection()
                } else {
                    if (lastReselection!!.second == p0.getItemId() && System.currentTimeMillis() - lastReselection!!.first <= 750) {
                        fragmentController?.clearStack()
                    }
                    lastReselection = null
                }
            }
        }

    private val onNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {

            override fun onNavigationItemSelected(p0: MenuItem): Boolean {
                lastReselection = null
                when (p0.itemId) {
                    R.id.navigation_tab1 -> {
                        fragmentController?.switchTab(FragmentController.TAB1)
                        return true
                    }
                    R.id.navigation_tab2 -> {
                        fragmentController?.switchTab(FragmentController.TAB2)
                        return true
                    }
                }
                return false
            }
        }

    override fun pushFragment(fragment: Fragment) {
        showProgress(false)
        fragmentController?.pushFragment(fragment)
    }

    private fun clearLastReselection() {
        Handler().postDelayed({ lastReselection = null }, 1250)
    }


    override fun getRootFragment(index: Int): Fragment {
        when (index) {
            FragmentController.TAB1 ->
                return WeatherNowFragment()
            FragmentController.TAB2 ->
                return WeatherDailyFragment()
        }
        throw IllegalStateException("Need to send an index that we know")
    }

    override fun onTabTransaction(fragment: Fragment?, index: Int) {
    }

    override fun onFragmentTransaction(
        fragment: Fragment?,
        transactionType: FragmentController.TransactionType
    ) {
        supportActionBar?.setDisplayHomeAsUpEnabled(!fragmentController!!.isRootFragment)
    }

    override fun switchTab(tabNum: Int) {
        fragmentController?.switchTab(tabNum)
        when (tabNum) {
            FragmentController.TAB1 -> navigation.selectedItemId = R.id.navigation_tab1
            FragmentController.TAB2 -> navigation.selectedItemId = R.id.navigation_tab2
        }
    }
}
