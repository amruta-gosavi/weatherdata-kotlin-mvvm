package app.com.weatherdata_kotlin_mvvm.presentation.support
import androidx.annotation.CheckResult
import androidx.annotation.IdRes
import androidx.annotation.IntDef
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.util.ArrayList
import java.util.Stack

import app.com.weatherdata_kotlin_mvvm.R

class FragmentController(builder: Builder) {
    @IdRes
    private val containerId: Int
    private val fragmentStacks: MutableList<Stack<Fragment>>
    private val fragmentManager: FragmentManager
    private val defaultTransactionOptions: TransactionOptions?
    @TabIndex
    private var selectedTabIndex: Int = 0
    private var tagCount: Int = 0
    private var currentFragment: Fragment? = null
    private val rootFragmentListener: RootFragmentListener?
    private val transactionListener: TransactionListener?
    private var executingTransaction: Boolean = false

    val currentFrag: Fragment?
        @CheckResult
        get() { if (currentFragment != null) {
                return currentFragment
            } else if (selectedTabIndex == NO_TAB) {
                return null
            } else {
                val fragmentStack = fragmentStacks[selectedTabIndex]
                if (!fragmentStack.isEmpty()) {
                    currentFragment =
                        fragmentManager.findFragmentByTag(fragmentStacks[selectedTabIndex].peek().tag) } }
            return currentFragment
        }

    val currentStack: Stack<Fragment>?
        @CheckResult
        get() = getStack(selectedTabIndex)

    val isRootFragment: Boolean
        @CheckResult
        get() {
            val stack = currentStack
            return stack == null || stack.size == 1 }
    init {
        fragmentManager = builder.fragmentManager
        containerId = builder.containerId
        fragmentStacks = ArrayList(builder.numTabs)
        rootFragmentListener = builder.rootFragmentListener
        transactionListener = builder.transactionListener
        defaultTransactionOptions = builder.defaultTransactionOptions
        selectedTabIndex = builder.mSelectedTabIndex
        for (i in 0 until builder.numTabs) {
            val stack = Stack<Fragment>()
            if (builder.rootFragments != null) {
                stack.add(builder.rootFragments!![i])
            }
            fragmentStacks.add(stack)
        }
        initialize(builder.mSelectedTabIndex)
    }

    @Throws(IndexOutOfBoundsException::class)
    @JvmOverloads
    fun switchTab(@TabIndex index: Int, transactionOptions: TransactionOptions? = null) {
        if (index >= fragmentStacks.size) {
            throw IndexOutOfBoundsException("Index : " + index + ", stack size : " + fragmentStacks.size)
        }
        if (selectedTabIndex != index) {
            selectedTabIndex = index
            val ft = createTransactionWithOptions(transactionOptions)
            detachCurrentFragment(ft)
            var fragment: Fragment? = null
            if (index == NO_TAB) {
                ft.commit()
            } else {
                fragment = reattachPreviousFragment(ft)
                if (fragment != null) {
                    ft.commit()
                } else {
                    fragment = getRootFragment(selectedTabIndex)
                    ft.add(containerId, fragment, generateTag(fragment))
                    ft.commit() }
            }
            executePendingTransactions()
            currentFragment = fragment
            transactionListener?.onTabTransaction(currentFragment, selectedTabIndex)
        }
    }

    @JvmOverloads
    fun pushFragment(fragment: Fragment?, transactionOptions: TransactionOptions? = null) {
        if (fragment != null && selectedTabIndex != NO_TAB) {
            val ft = createTransactionWithOptions(transactionOptions)
            ft.setCustomAnimations(
                R.animator.fragment_zoom_in, R.animator.fragment_zoom_out, R.animator.fragment_back_zoom_out, R.animator.fragment_back_zoom_in
            )
            try {
                detachCurrentFragment(ft)
                ft.add(containerId, fragment, generateTag(fragment))
                ft.addToBackStack(null)
                ft.commit()

            } catch (e: Exception) {
            }
            executePendingTransactions()
            fragmentStacks[selectedTabIndex].push(fragment)
            currentFragment = fragment
            transactionListener?.onFragmentTransaction(currentFragment, TransactionType.PUSH) }
    }

    @Throws(UnsupportedOperationException::class)
    @JvmOverloads
    fun popFragment(transactionOptions: TransactionOptions? = null) {
        popFragments(1, transactionOptions) }

    @Throws(UnsupportedOperationException::class)
    fun popFragments(popDepth: Int, transactionOptions: TransactionOptions?) {
        if (isRootFragment) {
            throw UnsupportedOperationException("cannot pop rootFragment")
        } else if (popDepth < 1) {
            throw UnsupportedOperationException("invalid popDepth")
        } else if (selectedTabIndex == NO_TAB) {
            throw UnsupportedOperationException("invalid current tab") }

        if (popDepth >= fragmentStacks[selectedTabIndex].size - 1) {
            clearStack(transactionOptions)
            return }

        var fragment: Fragment?
        val ft = createTransactionWithOptions(transactionOptions)
        ft.setCustomAnimations(
            R.animator.fragment_zoom_in, R.animator.fragment_zoom_out, R.animator.fragment_back_zoom_out, R.animator.fragment_back_zoom_in
        )
        for (i in 0 until popDepth) {
            fragment = fragmentManager.findFragmentByTag(fragmentStacks[selectedTabIndex].pop().tag)
            if (fragment != null) {
                ft.remove(fragment) }
        }
        fragment = reattachPreviousFragment(ft)

        var bShouldPush = false
        if (fragment != null) {
            ft.commit()
        } else {
            if (!fragmentStacks[selectedTabIndex].isEmpty()) {
                fragment = fragmentStacks[selectedTabIndex].peek()
                ft.add(containerId, fragment!!, fragment.tag)
                ft.commit()
            } else {
                fragment = getRootFragment(selectedTabIndex)
                ft.add(containerId, fragment, generateTag(fragment))
                ft.commit()
                bShouldPush = true }
        }
        executePendingTransactions()

        if (bShouldPush) {
            fragmentStacks[selectedTabIndex].push(fragment) }
        currentFragment = fragment
        transactionListener?.onFragmentTransaction(currentFragment, TransactionType.POP)
    }

    @JvmOverloads
    fun clearStack(transactionOptions: TransactionOptions? = null) {
        if (selectedTabIndex == NO_TAB) {
            return
        }
        val fragmentStack = fragmentStacks[selectedTabIndex]
        if (fragmentStack.size > 1) {
            var fragment: Fragment?
            val ft = createTransactionWithOptions(transactionOptions)

            while (fragmentStack.size > 1) {
                fragment = fragmentManager.findFragmentByTag(fragmentStack.pop().tag)
                if (fragment != null) {
                    ft.remove(fragment) }
            }
            fragment = reattachPreviousFragment(ft)
            var bShouldPush = false
            if (fragment != null) {
                ft.commit()
            } else {
                if (!fragmentStack.isEmpty()) {
                    fragment = fragmentStack.peek()
                    ft.add(containerId, fragment!!, fragment.tag)
                    ft.commit()
                } else {
                    fragment = getRootFragment(selectedTabIndex)
                    ft.add(containerId, fragment, generateTag(fragment))
                    ft.commit()

                    bShouldPush = true }
            }
            executePendingTransactions()
            if (bShouldPush) {
                fragmentStacks[selectedTabIndex].push(fragment)
            }
            fragmentStacks[selectedTabIndex] = fragmentStack
            currentFragment = fragment
            transactionListener?.onFragmentTransaction(currentFragment, TransactionType.POP)
        }
    }

    private fun initialize(@TabIndex index: Int) {
        selectedTabIndex = index
        if (selectedTabIndex > fragmentStacks.size) {
            throw IndexOutOfBoundsException("invalid index")
        }
        selectedTabIndex = index
        clearFragmentManager()
        if (index == NO_TAB) {
            return }
        val ft = createTransactionWithOptions(null)
        val fragment = getRootFragment(index)
        ft.add(containerId, fragment, generateTag(fragment))
        ft.commit()
        executePendingTransactions()
        currentFragment = fragment
        transactionListener?.onTabTransaction(currentFragment, selectedTabIndex)
    }

    @CheckResult
    @Throws(IllegalStateException::class)
    private fun getRootFragment(index: Int): Fragment {
        var fragment: Fragment? = null
        if (!fragmentStacks[index].isEmpty()) {
            fragment = fragmentStacks[index].peek()
        } else if (rootFragmentListener != null) {
            fragment = rootFragmentListener.getRootFragment(index)
            if (selectedTabIndex != NO_TAB) {
                fragmentStacks[selectedTabIndex].push(fragment) }
        }
        if (fragment == null) {
            throw IllegalStateException("invalid index")
        }
        return fragment
    }

    private fun reattachPreviousFragment(ft: FragmentTransaction): Fragment? {
        val fragmentStack = fragmentStacks[selectedTabIndex]
        var fragment: Fragment? = null
        if (!fragmentStack.isEmpty()) {
            fragment = fragmentManager.findFragmentByTag(fragmentStack.peek().tag)
            if (fragment != null) {
                ft.attach(fragment) }
        }
        return fragment
    }

    private fun detachCurrentFragment(ft: FragmentTransaction) {
        val oldFrag = currentFrag
        if (oldFrag != null) {
            ft.detach(oldFrag)
        }
    }

    @CheckResult
    private fun generateTag(fragment: Fragment): String {
        return fragment.javaClass.name + ++tagCount
    }

    private fun executePendingTransactions() {
        if (!executingTransaction) {
            executingTransaction = true
            try {
                fragmentManager.executePendingTransactions()
                executingTransaction = false
            } catch (e: Exception) {
            }
        }
    }

    private fun clearFragmentManager() {
        if (fragmentManager.fragments != null) {
            val ft = createTransactionWithOptions(null)
            for (fragment in fragmentManager.fragments) {
                if (fragment != null) {
                    ft.remove(fragment) }
            }
            ft.commit()
            executePendingTransactions()
        }
    }

    @CheckResult
    private fun createTransactionWithOptions(transactionOptions: TransactionOptions?): FragmentTransaction {
        var transactionOptions = transactionOptions
        val ft = fragmentManager.beginTransaction()
        if (transactionOptions == null) {
            transactionOptions = defaultTransactionOptions
        }
        if (transactionOptions != null) {
            ft.setCustomAnimations(
                transactionOptions.enterAnimation,
                transactionOptions.exitAnimation,
                transactionOptions.popEnterAnimation,
                transactionOptions.popExitAnimation
            )
            ft.setTransitionStyle(transactionOptions.transitionStyle)
            ft.setTransition(transactionOptions.transition)
            if (transactionOptions.sharedElements != null) {
                for (sharedElement in transactionOptions.sharedElements!!) {
                    ft.addSharedElement(sharedElement.first, sharedElement.second) }
            }
            if (transactionOptions.breadCrumbTitle != null) {
                ft.setBreadCrumbTitle(transactionOptions.breadCrumbTitle)
            }
            if (transactionOptions.breadCrumbShortTitle != null) {
                ft.setBreadCrumbShortTitle(transactionOptions.breadCrumbShortTitle)
            }
        }
        return ft
    }

    @CheckResult
    fun getStack(@TabIndex index: Int): Stack<Fragment>? {
        if (index == NO_TAB) return null
        if (index >= fragmentStacks.size) {
            throw IndexOutOfBoundsException("invalid index")
        }
        return fragmentStacks[index].clone() as Stack<Fragment>
    }

    enum class TransactionType { PUSH, POP }

    @IntDef(NO_TAB, TAB1, TAB2)
    @Retention(RetentionPolicy.SOURCE)
    annotation class TabIndex

    @IntDef(
        FragmentTransaction.TRANSIT_NONE, FragmentTransaction.TRANSIT_FRAGMENT_OPEN, FragmentTransaction.TRANSIT_FRAGMENT_CLOSE, FragmentTransaction.TRANSIT_FRAGMENT_FADE
    )
    @Retention(RetentionPolicy.SOURCE)
    internal annotation class Transit

    interface RootFragmentListener {
        fun getRootFragment(index: Int): Fragment
    }
    interface TransactionListener {
        fun onTabTransaction(fragment: Fragment?, index: Int)
        fun onFragmentTransaction(fragment: Fragment?, transactionType: TransactionType)
    }
    companion object {
        const val NO_TAB = -1
        const val TAB1 = 0
        const val TAB2 = 1
        private val MAX_NUM_TABS = 3
    }
}
