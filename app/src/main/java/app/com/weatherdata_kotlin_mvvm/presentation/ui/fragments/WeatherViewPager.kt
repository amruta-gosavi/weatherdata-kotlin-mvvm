package app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.adapters.DetailsViewPagerAdapter
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder

class WeatherViewPager(private val data: List<DisplayableWeatherData.DisplayableData>) :
    BaseFragment() {

    lateinit var unbinder: Unbinder

    @BindView(R.id.viewpager) lateinit var viewPager: ViewPager


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_view_pager
    }

    override fun initializeLayout() {
        showToolbar(true)
        setStatusBarColor(R.color.colorPrimaryDark)

        val adapter = DetailsViewPagerAdapter(fragmentManager!!)
        viewPager.setAdapter(adapter)
        adapter.setData(data as MutableList<DisplayableWeatherData.DisplayableData>)
        viewPager.invalidate()
    }

    fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        //cleanup
        unbinder.unbind()
        super.onDestroy()
    }
}


