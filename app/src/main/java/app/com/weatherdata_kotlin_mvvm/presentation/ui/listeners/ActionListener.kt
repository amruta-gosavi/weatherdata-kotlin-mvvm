package app.com.weatherdata_kotlin_mvvm.presentation.ui.listeners

interface ActionListener {

    fun onViewClicked(parentId:String)
}