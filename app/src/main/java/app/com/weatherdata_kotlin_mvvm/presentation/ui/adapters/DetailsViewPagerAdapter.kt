package app.com.weatherdata_kotlin_mvvm.presentation.ui.adapters

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.recyclerview.widget.RecyclerView
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.WeatherType
import app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments.WeatherDetailsFragment

class DetailsViewPagerAdapter(private val fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private var dailyData: MutableList<DisplayableWeatherData.DisplayableData>? = null
    //private var service: ProfileById? = null

    fun setData(data: MutableList<DisplayableWeatherData.DisplayableData>) {
        if (this.dailyData != null && !this.dailyData!!.isEmpty()) {
            this.dailyData!!.clear()
        }
        this.dailyData = data
        notifyDataSetChanged()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        return super.instantiateItem(container, position)
    }

    override fun finishUpdate(container: ViewGroup) {
        super.finishUpdate(container)
    }

    override fun getItem(position: Int): Fragment {
        return WeatherDetailsFragment(dailyData?.get(position)!!)
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        super.restoreState(state, loader)
    }

    override fun saveState(): Parcelable? {
        return super.saveState()
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return super.isViewFromObject(view, `object`)
    }

    override fun startUpdate(container: ViewGroup) {
        super.startUpdate(container)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        super.setPrimaryItem(container, position, `object`)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }

    override fun getCount(): Int {
        return if (dailyData == null) 0 else dailyData?.size!!
    }

    override fun getPageTitle(position: Int): CharSequence {
        return ""
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }
}
