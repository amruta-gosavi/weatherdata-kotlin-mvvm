package app.com.weatherdata_kotlin_mvvm.presentation.support

import android.annotation.SuppressLint
import android.util.Pair
import android.view.View

import androidx.annotation.AnimRes
import androidx.annotation.StyleRes
import androidx.fragment.app.FragmentTransaction

import java.util.ArrayList

class TransactionOptions private constructor(builder: Builder) {
    internal var sharedElements: List<Pair<View, String>>? = null
    @SuppressLint("WrongConstant")
    @app.com.weatherdata_kotlin_mvvm.presentation.support.FragmentController.Transit
    internal var transition = FragmentTransaction.TRANSIT_NONE
    @AnimRes
    internal var enterAnimation = 0
    @AnimRes
    internal var exitAnimation = 0
    @AnimRes
    internal var popEnterAnimation = 0
    @AnimRes
    internal var popExitAnimation = 0
    @StyleRes
    internal var transitionStyle = 0
    internal var breadCrumbTitle: String? = null
    internal var breadCrumbShortTitle: String? = null

    init {
        sharedElements = builder.sharedElements
        enterAnimation = builder.enterAnimation
        exitAnimation = builder.exitAnimation
    }

    class Builder private constructor() {
        val sharedElements: List<Pair<View, String>>? = null
        val enterAnimation: Int = 0
        val exitAnimation: Int = 0
    }
}