package app.com.weatherdata_kotlin_mvvm.presentation.ui.custom

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecycleLayoutManager : LinearLayoutManager {

    private var scrollInterface: ScrollInterface? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, orientation: Int, reverseLayout: Boolean) : super(
        context,
        orientation,
        reverseLayout
    ) {
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
    }

    fun setScrollInterface(scrollInterface: ScrollInterface) {
        this.scrollInterface = scrollInterface
    }

    override fun scrollVerticallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        state: RecyclerView.State
    ): Int {
        val scrollRange = super.scrollVerticallyBy(dx, recycler, state)
        val overscroll = dx - scrollRange
        if (overscroll > 0) {
            if (scrollInterface != null) {
                scrollInterface!!.overscrolled()
            }
        }
        return scrollRange
    }
}