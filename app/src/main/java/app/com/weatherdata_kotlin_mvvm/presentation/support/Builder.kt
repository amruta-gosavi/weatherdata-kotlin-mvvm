package app.com.weatherdata_kotlin_mvvm.presentation.support

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

class Builder(

    private val savedInstanceState: Bundle?,
    val fragmentManager: FragmentManager,
    val containerId: Int
) {
    var rootFragmentListener: FragmentController.RootFragmentListener? = null
    @FragmentController.TabIndex
    val mSelectedTabIndex = TAB1
    var transactionListener: FragmentController.TransactionListener? = null
    val defaultTransactionOptions: TransactionOptions? = null
    var numTabs = 0
    var rootFragments: List<Fragment>? = null

    fun rootFragments(rootFragments: List<Fragment>): Builder {
        this.rootFragments = rootFragments
        numTabs = rootFragments.size
        if (numTabs > MAX_NUM_TABS) {
            throw IllegalArgumentException("root fragments larger than $MAX_NUM_TABS")
        }
        return this
    }

    fun rootFragmentListener(
        listener: FragmentController.RootFragmentListener,
        numberOfTabs: Int
    ): Builder {
        this.rootFragmentListener = listener
        numTabs = numberOfTabs
        if (numTabs > MAX_NUM_TABS) {
            throw IllegalArgumentException("Number of tabs exceeds $MAX_NUM_TABS")
        }
        return this
    }

    fun transactionListener(`val`: FragmentController.TransactionListener): Builder {
        transactionListener = `val`
        return this
    }

    fun build(): FragmentController {
        if (rootFragmentListener == null && rootFragments == null) {
            throw IndexOutOfBoundsException("uninitialized")
        }
        return FragmentController(this)
    }

    companion object {
        const val NO_TAB = -1
        const val TAB1 = 0
        const val TAB2 = 1
        private val MAX_NUM_TABS = 3

        fun newBuilder(
            savedInstanceState: Bundle?,
            fragmentManager: FragmentManager,
            containerId: Int
        ): Builder {
            return Builder(savedInstanceState, fragmentManager, containerId)
        }

    }
}
