package app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import app.com.weatherdata_kotlin_mvvm.R
import butterknife.ButterKnife

/**
 * BaseFragment: handles default functionality, initialization of components
 */
abstract class BaseFragment : Fragment() {

    protected var pageTitle = context?.resources?.getString(R.string.app_name)
    protected var interactionListener: FragmentInteractionListener? = null
    protected var fragmentNavigation: FragmentNavigation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        if (interactionListener != null && pageTitle != "") {
            pageTitle?.let { interactionListener!!.updateTitle(it) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getFragmentLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectViews(view)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is FragmentInteractionListener) {
            interactionListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement FragmentInteractionListener")
        }
        if (context is FragmentNavigation) {
            fragmentNavigation = context
        }
    }

    private fun injectViews(view: View) {
        ButterKnife.bind(this, view)
        initializeLayout()
    }

    interface FragmentNavigation {
        fun pushFragment(fragment: Fragment)
    }

    fun showToolbar(shouldShow: Boolean) {
        if (shouldShow && activity != null) {
            activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.VISIBLE
        } else if (!shouldShow && activity != null) {
            activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        }
    }

    fun setStatusBarColor(colorRes: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity?.window?.statusBarColor = ContextCompat.getColor(context!!, colorRes)
            val decor = activity?.window?.decorView
            decor?.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                interactionListener?.popMe()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDetach() {
        super.onDetach()
        interactionListener = null
    }

    protected abstract fun getFragmentLayout(): Int
    protected abstract fun initializeLayout()
    interface FragmentInteractionListener {
        fun updateTitle(title: String)
        fun updateTitle(title: Int)
        fun popMe()
        fun showProgress(show: Boolean)
        fun switchTab(tabNum: Int)
    }


}