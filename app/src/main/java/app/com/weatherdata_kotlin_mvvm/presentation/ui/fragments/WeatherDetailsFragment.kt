package app.com.weatherdata_kotlin_mvvm.presentation.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.WeatherType
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder


class WeatherDetailsFragment(private val data: DisplayableWeatherData.DisplayableData) :
    BaseFragment() {


    lateinit var unbinder: Unbinder

    @BindView(R.id.tv_precipitation_value) lateinit var tvPrecipitationValue: TextView
    @BindView(R.id.tv_cloud_cover_value) lateinit var tvCloudCover: TextView
    @BindView(R.id.tv_precipintensity_value) lateinit var tvPrecipintensityValue: TextView
    @BindView(R.id.tv_pressure_value) lateinit var tvPressureValue: TextView
    @BindView(R.id.tv_speed_value) lateinit var tvSpeedValue: TextView
    @BindView(R.id.tv_gusts_value) lateinit var tvGustsValue: TextView
    @BindView(R.id.tv_sunrise_value) lateinit var tvSunriseValue: TextView
    @BindView(R.id.tv_sunset_value) lateinit var tvSunsetValue: TextView
    @BindView(R.id.tv_time) lateinit var tvTime: TextView
    @BindView(R.id.tv_temp) lateinit var tvTemp: TextView
    @BindView(R.id.iv_weather) lateinit var ivWeather: ImageView
    @BindView(R.id.tv_summary) lateinit var tvSummary: TextView


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(this, view)
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_weather_details
    }

    override fun initializeLayout() {
        showToolbar(true)
        setStatusBarColor(R.color.colorPrimaryDark)
        interactionListener?.updateTitle(R.string.app_name)

        setData()
    }

    fun setData() {
        if (data != null) {
            tvPrecipitationValue.text = data.precipProbability
            tvCloudCover.text = data.cloudCover
            tvPrecipintensityValue.text = data.precipIntensity
            tvPressureValue.text = data.pressure
            tvSpeedValue.text = data.windSpeed
            tvGustsValue.text = data.gusts
            tvSunriseValue.text = data.sunrise
            tvSunsetValue.text = data.sunset
            tvTime.text = data.day
            tvSummary.text = data.summary
            tvTemp.text = data.temperature
            tvSunriseValue.text = data.sunrise
            tvSunsetValue.text = data.sunset


            when (data.icon) {
                WeatherType.Clear_day.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_clear_day)
                }
                WeatherType.Partly_cloudy_day.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_partly_cloudy_day)
                }
                WeatherType.Partly_cloudy_night.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_party_cloudy_night)
                }
                WeatherType.Rain.stringValue() -> {
                    setImageDrawables(R.drawable.ic_weather_rain)
                }
            }
        }
    }

    fun setImageDrawables(drawable: Int) {
        ivWeather.setImageResource(drawable)
    }
}