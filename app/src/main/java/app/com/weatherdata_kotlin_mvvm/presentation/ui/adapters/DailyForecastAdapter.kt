package app.com.weatherdata_kotlin_mvvm.presentation.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.com.weatherdata_kotlin_mvvm.R
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import app.com.weatherdata_kotlin_mvvm.presentation.ui.common.WeatherType
import app.com.weatherdata_kotlin_mvvm.presentation.ui.listeners.ActionListener
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.annotations.Nullable
import java.util.*

class DailyForecastAdapter(private val context: Context, val actionListener: ActionListener) :
    RecyclerView.Adapter<DailyForecastAdapter.ViewHolder>() {

    private var itemList: MutableList<DisplayableWeatherData.DisplayableData> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(context).inflate(R.layout.item_daily_forecast, parent, false)
        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemList[position]

        holder.tv_temp.text = item.temperature
        holder.tv_title.text = item.summary
        holder.tv_time.text = item.day

        when (item.icon) {
            WeatherType.Clear_day.stringValue() -> {
                setImageDrawables(R.drawable.ic_weather_clear_day, holder)
            }
            WeatherType.Partly_cloudy_day.stringValue() -> {
                setImageDrawables(R.drawable.ic_weather_partly_cloudy_day, holder)
            }
            WeatherType.Partly_cloudy_night.stringValue() -> {
                setImageDrawables(R.drawable.ic_weather_party_cloudy_night, holder)
            }
            WeatherType.Rain.stringValue() -> {
                setImageDrawables(R.drawable.ic_weather_rain, holder)
            }
        }

        holder.itemView!!.setOnClickListener(View.OnClickListener {
            actionListener.onViewClicked(item.day!!)
        })
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun setImageDrawables(drawable: Int, holder: ViewHolder) {
        holder.iv_image.setImageResource(drawable)
    }

    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        @Nullable
        @BindView(R.id.iv_image)
        lateinit var iv_image: ImageView
        @Nullable
        @BindView(R.id.tv_title)
        lateinit var tv_title: TextView
        @Nullable
        @BindView(R.id.tv_temp)
        lateinit var tv_temp: TextView
        @Nullable
        @BindView(R.id.tv_time)
        lateinit var tv_time: TextView

        init {
            ButterKnife.bind(this, view)
        }
    }

    fun addItems(newItems: List<DisplayableWeatherData.DisplayableData>?) {
        if (newItems == null || newItems.isEmpty()) {
            return
        }
        val startPosition = itemList.size
        itemList.addAll(newItems)
        notifyItemRangeInserted(startPosition, newItems.size)
    }

    fun clearAll() {
        itemList.clear()
    }

}
