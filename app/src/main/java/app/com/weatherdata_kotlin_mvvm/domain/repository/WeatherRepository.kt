package app.com.weatherdata_kotlin_mvvm.domain.repository

import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import io.reactivex.Observable

/**
 *  This interface acts as a bridge between data and domain layer
 */
interface WeatherRepository {

    fun getWeatherForecast(
        lat: Double,
        lon: Double
    ): Observable<DisplayableWeatherData.DisplayableDarkSky>
}