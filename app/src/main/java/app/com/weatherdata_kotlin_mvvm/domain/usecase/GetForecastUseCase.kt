package app.com.weatherdata_kotlin_mvvm.domain.usecase

import app.com.weatherdata_kotlin_mvvm.domain.repository.WeatherRepository
import app.com.weatherdata_kotlin_mvvm.presentation.model.DisplayableWeatherData
import io.reactivex.Observable

/**
 * UseCase: Single responsibility class which retrives weather data from data layer
 */
class GetForecastUseCase(val weatherRepository: WeatherRepository) {

    //Wrapper for success/failure , error handling
    sealed class Result {
        object Loading : Result()
        data class Success(val weather: DisplayableWeatherData.DisplayableDarkSky) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(lat: Double, lon: Double): Observable<Result> {
        return weatherRepository.getWeatherForecast(lat, lon)
            .map {
                Result.Success(
                    it
                ) as Result
            }
            .onErrorReturn {
                Result.Failure(
                    it
                )
            }
            .startWith(Result.Loading)

    }
}
